# ActionMailerのメール送信方法を自作する

## 概要

ActionMailerのメール送信方法を自作する方法を記載したサンプルプロジェクトです。

このサンプルプロジェクトでは、例としてメール送信先としてSlackにメッセージをPOSTします。
事前にSlackでBotを作成し、APIトークンを取得しておいてください。

## テスト手順

    $ git clone ...
    $ cd action_mailer_my_delivery_method_test

    $ bundle install

    $ export SLACK_API_TOKEN="Slack APIのトークン"
    $ export SLACK_DEFAULT_CHANNEL="#my_channel" # デフォルトのPOSTするチャンネル

    $ rails r 'TestMailer.test_email.deliver'
    $ rails r 'TestMailer.test_email("#my_channel2").deliver'

非同期での送信も可能です。

    $ rails c
    > TestMailer.test_email.deliver_later
