class TestMailer < ApplicationMailer
  def test_email(channel = nil)
    mail to: "to@example.org",
         cc: 'cc@example.org',
         bcc: 'bcc@example.org',
         subject: 'テストメール',
         channel: channel
  end
end
