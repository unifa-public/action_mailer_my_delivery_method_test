require 'slack-ruby-client'

class SlackMailDeliveryMethod
  # @return [Hash]
  attr_accessor :settings

  # @param value [Hash]
  def initialize(value)
    self.settings = value
  end

  # @param mail [Mail::Message]
  def deliver!(mail)
    attachments = [to_attachment(mail)]
    channel = mail['channel']&.value.presence || self.settings[:channel]

    client = Slack::Web::Client.new(token: self.settings[:api_token])
    client.chat_postMessage(channel: channel, attachments: attachments)
  end

  private

  def to_attachment(mail)
    {
      title: mail.subject,
      text: mail.body.to_s,
      fields: [
        { title: 'From', value: mail.from_addrs.to_s },
        { title: 'To', value: mail.to_addrs.to_s },
        { title: 'Cc', value: mail.cc_addrs.to_s },
        { title: 'Bcc', value: mail.bcc_addrs.to_s },
      ],
    }
  end
end
